/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compra;

/**
 *
 * @author CompuStore
 */
public class Cliente {
    private String Nombre;
    private String Apellido;
    private int Cedula;
    private String Direccion;
    private int GasolinaSuper;
    private Double GasolinaExtra;
    private int CantidadDeGalones;
    private int Iva;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public int getCedula() {
        return Cedula;
    }

    public void setCedula(int Cedula) {
        this.Cedula = Cedula;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public int getGasolinaSuper() {
        return GasolinaSuper;
    }

    public void setGasolinaSuper() {
        this.GasolinaSuper = 2;
    }

    public Double getGasolinaExtra() {
        return GasolinaExtra;
    }

    public void setGasolinaExtra() {
        this.GasolinaExtra = 1.50;
    }

    public int getCantidadDeGalones() {
        return CantidadDeGalones;
    }

    public void setCantidadDeGalones(int CantidadDeGalones) {
        this.CantidadDeGalones = CantidadDeGalones;
    }

    public int getIva() {
        return Iva;
    }

    public void setIva() {
        this.Iva = (12/100);
  
   }
public double SubtotalExtra(){
        return this.GasolinaExtra *this.CantidadDeGalones;
        
}
public double TotalExtra(){
    return this.SubtotalExtra()*Iva;
}
        public int SubTotalSuper(){
          return this.GasolinaSuper*this.CantidadDeGalones;  
        }
        public int TotalSuper(){
          return this.SubTotalSuper()*Iva;   
        }
    }
    

